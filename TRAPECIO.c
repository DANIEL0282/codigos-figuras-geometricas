#include <stdio.h>

int main()
{
    float Base1, Base2, Altura, Area;
    char opcion = 'Y'; // Utilizar comillas simples y un solo carácter
    
    while (opcion == 'Y' || opcion == 'y') { // Utilizar comillas simples para caracteres
        
        printf("Introduzca la longitud de la base mayor: ");
        scanf("%f", &Base1);
        
        printf("Introduzca la longitud de la base menor: ");
        scanf("%f", &Base2);
        
        printf("Introduzca la altura del trapezoide: ");
        scanf("%f", &Altura);
        
        Area = ((Base1 + Base2) * Altura) / 2.0;
        
        printf("El resultado de su consulta es %.2f\n", Area);
        
    
        
        printf("Desea hacer un nuevo calculo? (Y/N): ");
        scanf("  %c", &opcion);
        
    }
    
    printf("Gracias por su consulta\n");
    
    return 0;
}