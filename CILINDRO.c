#include <stdio.h>

int main()
{
    float radio, altura, area_base, area_lateral, area_total;
    float pi = 3.14159; // Se define pi
    
    printf("INGRESE EL RADIO DE LA BASE DE SU CILINDRO: ");
    scanf("%f", &radio);
    
    printf("INGRESE LA ALTURA DE SU CILINDRO: ");
    scanf("%f", &altura);
    
    //Aca se calcula el area
    area_total = 2 * pi * (radio * radio) + (2 * pi * radio * altura);
    
    printf("El area total de su cilindro es : %.5f",area_total);
    
    
    return 0;
    
}